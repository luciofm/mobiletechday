package com.luciofm.mobiletechday;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.luciofm.mobiletechday.slides.FirstFragment;

public class SlidesAdapter extends FragmentPagerAdapter {

	String[] pages;

	public SlidesAdapter(FragmentManager fm, String[] pages) {
		super(fm);
		this.pages = pages;
	}

	@Override
	public Fragment getItem(int position) {
		Fragment frag = null;
		try {
			frag = (Fragment) Class.forName(pages[position]).newInstance();
			return frag;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new FirstFragment();
	}

	@Override
	public int getCount() {
		return pages.length;
	}
}
