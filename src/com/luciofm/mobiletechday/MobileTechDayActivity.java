package com.luciofm.mobiletechday;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

public class MobileTechDayActivity extends FragmentActivity {

	ViewPager mPager;
	SlidesAdapter mAdapter;

	/* Slide Classes to Show... */
	String pages[] = { 
			"com.luciofm.mobiletechday.slides.FirstFragment",
			"com.luciofm.mobiletechday.slides.WhoFragment",
			"com.luciofm.mobiletechday.slides.History1Fragment",
			"com.luciofm.mobiletechday.slides.History2Fragment",
			"com.luciofm.mobiletechday.slides.EvolutionFragment",
			"com.luciofm.mobiletechday.slides.SDKFragment",
			"com.luciofm.mobiletechday.slides.ADTFragment",
			"com.luciofm.mobiletechday.slides.MarketFragment",
			"com.luciofm.mobiletechday.slides.ArchitectureFragment",
			"com.luciofm.mobiletechday.slides.ConceptsFragment",
			"com.luciofm.mobiletechday.slides.ActivityFragment",
			"com.luciofm.mobiletechday.slides.HelloWorldFragment",
			"com.luciofm.mobiletechday.slides.LifeCycleFragment",
			"com.luciofm.mobiletechday.slides.ServiceFragment",
			"com.luciofm.mobiletechday.slides.Intent1Fragment",
			"com.luciofm.mobiletechday.slides.SharingFragment",
			"com.luciofm.mobiletechday.slides.BroadcastFragment",
			"com.luciofm.mobiletechday.slides.ManifestFragment",
			"com.luciofm.mobiletechday.slides.InterfaceFragment",
			"com.luciofm.mobiletechday.slides.RelativeFragment",
			"com.luciofm.mobiletechday.slides.RelativeExampleFragment",
			"com.luciofm.mobiletechday.slides.LinearLayoutFragment",
			"com.luciofm.mobiletechday.slides.LinearLayoutExampleFragment",
			"com.luciofm.mobiletechday.slides.FoursquareFragment",
			"com.luciofm.mobiletechday.slides.FoursquareSourceFragment",
			"com.luciofm.mobiletechday.slides.AjudaFragment",
			"com.luciofm.mobiletechday.slides.SourceFragment"};
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        getActionBar().hide();
        mPager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new SlidesAdapter(getSupportFragmentManager(), pages);
        mPager.setAdapter(mAdapter);
    }
}