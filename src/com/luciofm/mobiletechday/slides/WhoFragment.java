package com.luciofm.mobiletechday.slides;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;

public class WhoFragment extends Fragment {

	View slide;
	private ImageView android;
	private TextView title;
	private boolean animated = false;

	private TextView name;
	private TextView twitter;
	private TextView site;
	private TextView plus;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.who_frag, container, false);
		android = (ImageView) slide.findViewById(R.id.androidImage);
		title = (TextView) slide.findViewById(R.id.title);
		android.setTranslationX(500f);
		android.setTranslationY(250f);
		android.setRotation(-45f);
		
		name = (TextView) slide.findViewById(R.id.textName);
		twitter = (TextView) slide.findViewById(R.id.textTwitter);
		site = (TextView) slide.findViewById(R.id.textSite);
		plus = (TextView) slide.findViewById(R.id.textPlus);
		animated = false;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (!animated) {
					animated = true;
					animateTransition();
				}
			}
		});
	}

	public void animateTransition() {
		final ObjectAnimator animator = ObjectAnimator.ofFloat(title,
				"translationY", 0f, -200f); // .setDuration(700);
		animator.addListener(new AnimatorListener() {
			public void onAnimationStart(Animator animation) {
			}

			public void onAnimationRepeat(Animator animation) {
			}

			public void onAnimationEnd(Animator animation) {
				ObjectAnimator fade1 = ObjectAnimator.ofFloat(name, "alpha", 0f, 1f);
				ObjectAnimator fade2 = ObjectAnimator.ofFloat(twitter, "alpha", 0f, 1f);
				ObjectAnimator fade3 = ObjectAnimator.ofFloat(site, "alpha", 0f, 1f);
				ObjectAnimator fade4 = ObjectAnimator.ofFloat(plus, "alpha", 0f, 1f);
				AnimatorSet animSet = new AnimatorSet();
				animSet.playSequentially(fade1, fade2, fade3, fade4);
				animSet.start();
			}

			public void onAnimationCancel(Animator animation) {
			}
		});
		animator.start();

		/*ObjectAnimator moveRight = ObjectAnimator.ofFloat(android,
				"translationX", 0f, 500f);
		ObjectAnimator moveDown = ObjectAnimator.ofFloat(android,
				"translationY", 0f, 250f);
		ObjectAnimator rotate = ObjectAnimator.ofFloat(android, "rotation",
				-45f);
		AnimatorSet animSet = new AnimatorSet();
		animSet.play(moveRight).with(moveDown).with(rotate);
		animSet.addListener(new AnimatorListener() {
			public void onAnimationStart(Animator animation) {
			}

			public void onAnimationRepeat(Animator animation) {
			}

			public void onAnimationEnd(Animator animation) {
				animator.start();
			}

			public void onAnimationCancel(Animator animation) {
			}
		});
		animSet.start();*/
	}
}
