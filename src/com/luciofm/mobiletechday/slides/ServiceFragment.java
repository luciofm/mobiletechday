package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;

public class ServiceFragment extends Fragment {
	private View slide;
	
	private TextView title;
	private View[] texts;
	private int current = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.service, container, false);

		texts = new TextView[6];
		title = (TextView) slide.findViewById(R.id.textView1);
		texts[0] = (View) slide.findViewById(R.id.textView2);
		texts[1] = (View) slide.findViewById(R.id.textView3);
		texts[2] = (View) slide.findViewById(R.id.textView4);
		texts[3] = (View) slide.findViewById(R.id.textView5);
		texts[4] = (View) slide.findViewById(R.id.textView6);
		texts[5] = (View) slide.findViewById(R.id.textView7);
		current = -1;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (current < 0) {
					final ObjectAnimator animator = ObjectAnimator.ofFloat(title,
							"translationY", 0f, -200f);
					animator.start();
					current++;
				}
				if (current < texts.length)
					ObjectAnimator.ofFloat(texts[current++], "alpha", 1f).start();
			}
		});
	}
}
