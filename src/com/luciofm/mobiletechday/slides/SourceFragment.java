package com.luciofm.mobiletechday.slides;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;

public class SourceFragment extends Fragment {

	View slide;
	private ImageView android;
	private TextView title;
	private boolean animated = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.source, container, false);
		android = (ImageView) slide.findViewById(R.id.androidImage);
		title = (TextView) slide.findViewById(R.id.title);
		animated = false;
		
		android.setTranslationX(500f);
		android.setTranslationY(250f);
		android.setRotation(-45f);

		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (!animated) {
					animated = true;
					animateTransition();
				}
			}
		});
	}

	public void animateTransition() {
		final ObjectAnimator animator = ObjectAnimator.ofFloat(title,
				"translationX", -800f, 0f); // .setDuration(700);
		// animator.setInterpolator(new AccelerateInterpolator());
		animator.addListener(new AnimatorListener() {
			public void onAnimationStart(Animator animation) {
				title.setVisibility(View.VISIBLE);
			}

			public void onAnimationRepeat(Animator animation) {
			}

			public void onAnimationEnd(Animator animation) {
			}

			public void onAnimationCancel(Animator animation) {
			}
		});

		ObjectAnimator moveRight = ObjectAnimator.ofFloat(android,
				"translationX", 500f, 0f);
		ObjectAnimator moveDown = ObjectAnimator.ofFloat(android,
				"translationY", 250f, 0f);
		ObjectAnimator rotate = ObjectAnimator.ofFloat(android, "rotation",
				-45f, 0f);
		AnimatorSet animSet = new AnimatorSet();
		animSet.play(moveRight).with(moveDown).with(rotate);
		animSet.addListener(new AnimatorListener() {
			public void onAnimationStart(Animator animation) {
			}

			public void onAnimationRepeat(Animator animation) {
			}

			public void onAnimationEnd(Animator animation) {
				animator.start();
			}

			public void onAnimationCancel(Animator animation) {
			}
		});
		animSet.start();

		/*
		 * final ObjectAnimator animator =
		 * ObjectAnimator.ofPropertyValuesHolder(android,
		 * PropertyValuesHolder.ofFloat("translationX", 0f, 400f)); final
		 * LayoutParams lp = android.getLayoutParams();
		 * animator.addUpdateListener(new AnimatorUpdateListener() {
		 * 
		 * @Override public void onAnimationUpdate(ValueAnimator animation) {
		 * //lp.width = (Integer) animator.getAnimatedValue();
		 * //android.setLayoutParams(lp); } }); animator.start();
		 */
	}
}
