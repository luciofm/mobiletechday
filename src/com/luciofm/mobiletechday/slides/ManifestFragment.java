package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;
import com.luciofm.mobiletechday.Utils;

public class ManifestFragment extends Fragment {
	private View slide;
	
	private TextView title;
	private View[] views;
	private int current = -1;
	
	private int lastTouchEvent = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.manifest, container, false);

		views = new View[2];
		title = (TextView) slide.findViewById(R.id.textView1);
		views[0] = (View) slide.findViewById(R.id.webView1);

		((WebView)views[0]).loadDataWithBaseURL(null, Utils.readTemplate(getActivity(), R.raw.manifest_html), "text/html", "UTF-8", null);
		current = -1;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		((WebView) views[0]).setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View arg0, MotionEvent event) {
				Log.d("ManifestFragment", "on touch " + event.getAction());
				if (event.getAction() == 1 && (lastTouchEvent == 2 || lastTouchEvent == 0)) {
					if (current < 0) {
						final ObjectAnimator animator = ObjectAnimator.ofFloat(
								title, "translationY", 0f, -200f);
						animator.start();
						current++;
					}
					if (current < views.length && views[current] != null)
						ObjectAnimator.ofFloat(views[current++], "alpha", 1f)
								.start();
				}

				lastTouchEvent = event.getAction();
				return false;
			}
		});
	}
}
