package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.luciofm.mobiletechday.HelloWorldActivity;
import com.luciofm.mobiletechday.R;
import com.luciofm.mobiletechday.Utils;

public class HelloWorldFragment extends Fragment {
	private View slide;
	
	private TextView title;
	private View[] views;
	private int current = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.helloworld, container, false);

		views = new View[2];
		title = (TextView) slide.findViewById(R.id.textView1);
		views[0] = (View) slide.findViewById(R.id.webView1);
		views[1] = (View) slide.findViewById(R.id.button1);

		((WebView)views[0]).loadDataWithBaseURL(null, Utils.readTemplate(getActivity(), R.raw.activity_html), "text/html", "UTF-8", null);
		((Button)views[1]).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), HelloWorldActivity.class);
				startActivity(intent);
			}
		});
		current = -1;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (current < 0) {
					final ObjectAnimator animator = ObjectAnimator.ofFloat(title,
							"translationY", 0f, -200f);
					animator.start();
					current++;
				}
				if (current < views.length)
					ObjectAnimator.ofFloat(views[current++], "alpha", 1f).start();
			}
		});
	}
}
