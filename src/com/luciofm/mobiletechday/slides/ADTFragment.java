package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;

public class ADTFragment extends Fragment {
	private View slide;

	private TextView[] texts;
	private int current = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.adt, container, false);

		texts = new TextView[6];
		texts[0] = (TextView) slide.findViewById(R.id.textView2);
		texts[1] = (TextView) slide.findViewById(R.id.textView3);
		texts[2] = (TextView) slide.findViewById(R.id.textView4);
		texts[3] = (TextView) slide.findViewById(R.id.textView5);
		texts[4] = (TextView) slide.findViewById(R.id.textView6);
		texts[5] = (TextView) slide.findViewById(R.id.textView7);
		current = 0;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (current < texts.length)
					ObjectAnimator.ofFloat(texts[current++], "alpha", 1f).start();
			}
		});
	}
}
