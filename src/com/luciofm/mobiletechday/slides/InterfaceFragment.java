package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luciofm.mobiletechday.R;

public class InterfaceFragment extends Fragment {
	private View slide;
	
	private TextView title;
	private View[] views;
	private int current = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.interfaces, container, false);

		views = new TextView[6];
		title = (TextView) slide.findViewById(R.id.textView1);
		views[0] = (View) slide.findViewById(R.id.textView2);
		views[1] = (View) slide.findViewById(R.id.textView3);
		views[2] = (View) slide.findViewById(R.id.textView4);
		views[3] = (View) slide.findViewById(R.id.textView5);
		views[4] = (View) slide.findViewById(R.id.textView6);
		views[5] = (View) slide.findViewById(R.id.textView7);
		current = -1;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (current < 0) {
					final ObjectAnimator animator = ObjectAnimator.ofFloat(title,
							"translationY", 0f, -200f);
					animator.start();
					current++;
				}
				if (current < views.length && views[current] != null)
					ObjectAnimator.ofFloat(views[current++], "alpha", 1f).start();
			}
		});
	}
}
