package com.luciofm.mobiletechday.slides;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.luciofm.mobiletechday.HelloWorldActivity;
import com.luciofm.mobiletechday.R;
import com.luciofm.mobiletechday.Utils;

public class LifeCycleFragment extends Fragment {
	private View slide;
	
	private TextView title;
	private View[] views;
	private int current = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.lifecycle, container, false);

		views = new View[1];
		title = (TextView) slide.findViewById(R.id.textView1);
		views[0] = (View) slide.findViewById(R.id.ImageView1);

		current = -1;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(clickListener);
	}
	
	OnClickListener clickListener = new OnClickListener() {
		
		public void onClick(View v) {
			if (current < 0) {
				ObjectAnimator anim1 = ObjectAnimator.ofFloat(title,
						"translationY", 0f, -200f);
				ObjectAnimator anim2 = ObjectAnimator.ofFloat(title, "alpha", 0f);
				ObjectAnimator anim3 = ObjectAnimator.ofFloat(views[0], "alpha", 1f);
				AnimatorSet animSet = new AnimatorSet();
				animSet.playSequentially(anim1, anim2, anim3);
				animSet.start();
				current++;
			}
		}
	};
}
