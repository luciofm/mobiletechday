package com.luciofm.mobiletechday.slides;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.luciofm.mobiletechday.R;

public class EvolutionFragment extends Fragment {
	private View slide;
	
	private ImageView[] images;
	private int current = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		slide = inflater.inflate(R.layout.evolution, container, false);

		images = new ImageView[4];
		images[0] = (ImageView) slide.findViewById(R.id.imageView1);
		images[1] = (ImageView) slide.findViewById(R.id.imageView2);
		images[2] = (ImageView) slide.findViewById(R.id.imageView3);
		images[3] = (ImageView) slide.findViewById(R.id.imageView4);
		current = 0;
		
		return slide;
	}

	@Override
	public void onResume() {
		super.onResume();

		slide.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				if (current < images.length)
					ObjectAnimator.ofFloat(images[current++], "alpha", 1f).start();
			}
		});
	}
}
