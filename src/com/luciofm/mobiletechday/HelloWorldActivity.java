package com.luciofm.mobiletechday;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class HelloWorldActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TextView text = new TextView(this);
		text.setText("Hello World!!! I am an Activity!!!");
		text.setTextSize(60f);
		setContentView(text);
	}

}
